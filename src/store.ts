import { Image } from './app/image/image.component';
import { SliderActions } from './app/app.actions';

export interface IAppState {
    images: Array<Image>;
    currentSlide: number;
}

export const INITIAL_STATE: IAppState = {
    images: [],
    currentSlide: 0,
};

export function rootReducer(lastState: IAppState, action): IAppState {
    switch (action.type) {
        case SliderActions.NEXT: 
            return { 
                ...lastState,
                currentSlide: lastState.currentSlide + 1,
            };
        case SliderActions.PREV: 
            return { 
                ...lastState,
                currentSlide: lastState.currentSlide - 1,
            };
        case SliderActions.LOADITEMS: 
            return { 
                ...lastState,
                images: [...action.payload]
            };
        case SliderActions.RESET: 
            return { 
                ...lastState,
                currentSlide: 0,
            };
    }

    return lastState;
}