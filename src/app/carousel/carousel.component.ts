import { KEY_CODE } from './../app.component';
import { Image } from './../image/image.component';
import { IAppState } from './../../store';
import { SliderActions } from './../app.actions';
import { Component, OnInit, HostListener } from '@angular/core';
import { Observable } from 'rxjs';
import { NgRedux, select } from '@angular-redux/store';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})

export class CarouselComponent implements OnInit {

  @select() readonly currentSlide$: Observable<number>;
  @select() images$: Observable<Array<Image>>;

  images;
  currentSlide = 0;
  
  constructor(
    private ngRedux: NgRedux<IAppState>,
    private actions: SliderActions,
  ) { }

  ngOnInit() {
    this.currentSlide$.subscribe(state => {
      this.currentSlide = state;
    })
    this.images$.subscribe(state => {
      this.images = state[this.currentSlide];
    })
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {

    if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
      this.next();
    }

    if (event.keyCode === KEY_CODE.LEFT_ARROW) {
      this.prev();
    }
  }

  next() {
    this.ngRedux.dispatch(this.actions.next());
    this.images$.subscribe(state => {
      if (this.currentSlide >= state.length) {
        this.reset();
        this.images = state[this.currentSlide];
      } else {
        this.images = state[this.currentSlide];
      }
    })
  }
  
  prev() {
    this.ngRedux.dispatch(this.actions.prev());
    this.images$.subscribe(state => {
      this.images = state[this.currentSlide];
      if (this.currentSlide < 0) {
        this.reset();
        this.images = state[this.currentSlide];
      } else {
        this.images = state[this.currentSlide];
      } 
    })
  }

  reset() {
    this.ngRedux.dispatch(this.actions.reset());
    this.currentSlide$.subscribe(state => {
      this.currentSlide = state;
    })
  }

  getStyle() {
    return {
      'background-image': 'url('+this.images.src+')',
    }
  }
}
