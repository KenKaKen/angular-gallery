import { Image } from './../image/image.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-image-fullscreen',
  templateUrl: './image-fullscreen.component.html',
  styleUrls: ['./image-fullscreen.component.scss']
})
export class ImageFullscreenComponent implements OnInit {

  @select('images') images$: Observable<Array<Image>>;
  picture;
  images;

  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.images$.subscribe(state => {
      this.images = state;
    })

    this.route.paramMap.subscribe(params => {
      this.picture = this.images[+params.get('imageId')]
    });
  }

}
