import { Injectable } from "@angular/core";

@Injectable()
export class SliderActions {
    static NEXT = 'NEXT';
    static PREV = 'PREV';
    static LOADITEMS = 'LOADITEMS';
    static RESET = 'RESET';

    next() {
        return { type: SliderActions.NEXT };
    }

    prev() {
        return { type: SliderActions.PREV };
    }

    loadItems(payload) {
        return {
            type: SliderActions.LOADITEMS,
            payload
        };
    }
    reset() {
        return { type: SliderActions.RESET };
    }
}
