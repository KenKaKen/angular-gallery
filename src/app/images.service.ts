import { HttpClient } from '@angular/common/http';
import { SliderActions } from './app.actions';
import { NgRedux } from '@angular-redux/store';
import { Image } from './image/image.component';
import { Injectable } from '@angular/core';
import { IAppState } from '../store';

@Injectable({
  providedIn: 'root'
})

export class ImagesService {

  constructor(
    private ngRedux: NgRedux<IAppState>,
    private sliderActions: SliderActions,
    private http: HttpClient,
  ) { }

  getImages() {
     this.http
      .get('/assets/image.json')
      .subscribe((images: Array<Image>) => {
        this.ngRedux.dispatch(this.sliderActions.loadItems(images));
      });
  }
}
