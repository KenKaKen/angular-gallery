import { select } from '@angular-redux/store';
import { Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs';

export interface Image {
  src: string;
  alt: string;
}

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  images;

  constructor() { }

  @select('images') images$: Observable<Array<Image>>;

  ngOnInit() {
    this.images$.subscribe(state => {
      this.images = state;
    })
  }

}
