import { ImagesService } from './images.service';
import { RouterModule, Routes } from '@angular/router';
import { IAppState, INITIAL_STATE } from './../store';
import { SliderActions } from './app.actions';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import { rootReducer } from '../store';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { GalleryComponent } from './gallery/gallery.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ImageFullscreenComponent } from './image-fullscreen/image-fullscreen.component';
import { ImageComponent } from './image/image.component';

const appRoutes: Routes = [
  { path: 'image/:imageId', component: ImageFullscreenComponent },
  { path: '', component: MainComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    GalleryComponent,
    CarouselComponent,
    ImageFullscreenComponent,
    ImageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgReduxModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [SliderActions, ImagesService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>, devTools: DevToolsExtension) {
    const storeEnhancers = devTools.isEnabled() 
      ? [devTools.enhancer()]
      : [];
      
      ngRedux.configureStore(
        rootReducer,
        INITIAL_STATE,
        [],
        storeEnhancers,
      ); 
  }
}
